0.1.8
  - Added in the ability send a message to the application
  
0.1.7
  - Allow the ability to pass a pushState to the parent

0.1.6
  - Add in a animated scroll to top function

0.1.5
  - Allow users to specify a file path

0.1.4
  -

0.1.3
  - Added ability to prepopulate dimensions

0.1.2
  - Add in a watch function for the iframe height

0.1.1
  - Add in ability to pass data attributes through to the iframe

0.1.0:
  - Initial release of Mashboxjs
