(function () {
  var $ = document.querySelectorAll.bind(document);

  Node.prototype.find = function(name) {
    return this.querySelectorAll(name);
  };

  Node.prototype.on = window.on = function(name, fn) {
    this.addEventListener(name, fn);
  };

  NodeList.prototype.__proto__ = Array.prototype;

  NodeList.prototype.on = NodeList.prototype.addEventListener = function(name, fn) {
    this.forEach(function(elem, i) {
      elem.on(name, fn);
    });
  };

  NodeList.prototype.find = function(name) {
    var results = [];
    this.forEach(function(elem, i) {
      elem.find(name).forEach(function(child, i) {
        results.push(child);
      });
    });
    return results;
  };

  // Requirify
  if (typeof define === 'function' && define.amd) {
    define([], $);

    // Browserify
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = $;

  }

})();