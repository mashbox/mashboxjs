module.exports = function (obj, evt, fn) {
  if ('addEventListener' in window) {
      obj.addEventListener(evt, fn, false);

    } else if ('attachEvent' in window){
      obj.attachEvent('on'+ evt, fn);
    }
};