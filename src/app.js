// This will render an application in a frame

Math.easeInOutQuad = function (t, b, c, d) {
  t /= d/2;
  if (t < 1) { return c/2*t*t + b; }
  t--;
  return -c/2 * (t*(t-2) - 1) + b;
};

function Application($el) {
  var self = this;
  var config= {
    domain: "mashbox.com"
  };

  self.$   = require('bling');
  self.$el = $el;
  self.app = $el.getAttribute("data-app");

  self.params = {};
  self.queryString = [];

  Array.prototype.slice.call($el.attributes).forEach(function(item) {
    self.params[item.name.replace('data-', '')] = item.value;

    if (item.name.indexOf('data-') > -1 && item.name !== 'data-app') {
      self.queryString.push(item.name.replace('data-', '') + "=" + item.value);
    }
  });

  if (window.location.search.substr(1)) {
    self.queryString.unshift(window.location.search.substr(1));
  }

  self.queryString.push("origin=" + encodeURIComponent(window.location.href));

  if (self.app) {
    var split = self.app.split("/");
    if (split.length > 1) {
      self.customer = split[0];
      self.project = split[1];
      self.page = split[2] || "";
      self.path = self.getPath("//" + self.customer + "." + config.domain + "/" + self.project + '/' + self.page, self.queryString);

    } else {
      self.customer = "";
      self.project = split[0];
      self.path = self.getPath("helper/" + self.project, self.queryString);

    }

  } else {
    self.app = $el.getAttribute("data-test");
    self.path = self.getPath(self.app, self.queryString);
  }

  // Render the frame
  self.render();

  // Setup events
  self.eventHandler();

  return self;
}

Application.prototype.getPath = function (domain, queryString) {
  return domain + '?' + queryString.join('&');
};

Application.prototype.render = function () {
  var self = this;
  var tmpl = "<iframe src='" + self.path + "' scrolling='no' frameborder='0' allowfullscreen seamless></iframe>";

  if (!self.$el.find('iframe').length) {
    // Insert the frame
    self.$el.innerHTML = tmpl;
  }

  // Set the initial sizing
  self.frame = self.$el.find('iframe')[0];
  self.frame.style.width = self.params.width || '100%';
  self.frame.style.height = self.params.height || '300px';
};

Application.prototype.eventHandler = function () {
  var self = this;
  var addListener = require("addEventListener");

  addListener(window, "message", function (message) {
    self.receiveMessage(message);
  });
};

Application.prototype.sendMessage = function (message) {
  var self = this;

  window.Mashbox.Messages.sendMessage(self.frame.contentWindow, message);
};

Application.prototype.receiveMessage = function (message) {
  var self = this;

  try {
    var mbData = JSON.parse(message.data).mbData || {};

    // Resize the height
    if (mbData.height && self.frame) {
      self.frame.style.height = String(mbData.height).indexOf("%") > -1 ? mbData.height : parseInt(mbData.height, 10) + 'px';
      return;
    }

    // Scroll
    if (mbData.hasOwnProperty("scroll") && self.frame) {
      self.scrollTo(mbData.scroll, 600);
      return;
    }

    // Push State
    if (mbData.hasOwnProperty("pushState") && self.frame) {
      if (typeof(window.history.pushState) == 'function') {
        history.pushState(null, null, mbData.pushState);
      } else {
        window.location.href = mbData.pushState; 
      }
      return;
    }

  } catch (e) {
    console.warn("There was a problem with the frame message", e);
  }
};

Application.prototype.scrollTo = function (to, duration) {
    var start = document.documentElement.scrollTop || document.body.scrollTop;
    var change = to - start;
    var currentTime = 0;
    var increment = 20;

    var animateScroll = function (){
      currentTime += increment;
      var val = Math.easeInOutQuad(currentTime, start, change, duration);
      if (document.documentElement) {
        document.documentElement.scrollTop = val;
      }

      if (document.body) {
        document.body.scrollTop = val;
      }

      if (currentTime < duration) {
        setTimeout(animateScroll, increment);
      }
    };
    animateScroll();
};

module.exports = Application;
