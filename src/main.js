/*! MashboxJS 0.1.8 | (c) 2016 Mashbox LLC */
(function(window, document) {

  docReady = require('docready');

  // Set the global variable
  function Mashbox() {
    var self = this;

    self.$ = require('bling');

    // Add the App renderer
    self.App = require('app');

    // Add the Poller
    self.Poller = require('poller');

    // Add the Messages
    self.Messages = require('messages');

    // Set the Active Applications for reference later
    self.activeApplications = [];

    // Render the Apps
    window.docReady(function() {
      var $apps = self.$('.mashbox-app');
      if ($apps.length) {
        for (var x = 0; x < $apps.length; x = x + 1) {
          self.activeApplications.push(new self.App($apps[x]));
        }
      }

      // Trigger a loaded event
      if (typeof (window.MashboxLoaded) === 'function') {
        window.MashboxLoaded.call(self);
      }
    });

    return self;
  }

  window.Mashbox = new Mashbox();

  // Requirify
  if (typeof define === 'function' && define.amd) {
    define([], window.Mashbox);

    // Browserify
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = window.Mashbox;

  }

})(window, document);
