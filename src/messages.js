module.exports = {
  setSize: function (height) {
    var self = this;
    self.frameHeight = 0;

    var getComputedStyle = function (prop,el) {
  		function convertUnitsToPxForIE8(value) {
  			var PIXEL = /^\d+(px)?$/i;

  			if (PIXEL.test(value)) {
  				return parseInt(value, 10);
  			}

  			var style = el.style.left;
  			var runtimeStyle = el.runtimeStyle.left;

  			el.runtimeStyle.left = el.currentStyle.left;
  			el.style.left = value || 0;
  			value = el.style.pixelLeft;
  			el.style.left = style;
  			el.runtimeStyle.left = runtimeStyle;

  			return value;
  		}

  		var retVal = 0;
  		el =  el || document.body;

  		/* istanbul ignore else */ // Not testable in phantonJS
  		if (('defaultView' in document) && ('getComputedStyle' in document.defaultView)) {
  			retVal = document.defaultView.getComputedStyle(el, null);
  			retVal = (null !== retVal) ? retVal[prop] : 0;
  		} else {//IE8
  			retVal =  convertUnitsToPxForIE8(el.currentStyle[prop]);
  		}

  		return parseInt(retVal, 10);
  	};

    height = height || Math.max(document.body.offsetHeight + getComputedStyle('marginTop', document.body) + getComputedStyle('marginBottom', document.body), (document.documentElement.scrollHeight || document.body.scrollHeight));

    if (self.frameHeight !== height) {
      self.sendMessage(window.parent, {
        height: height
      });
    }
  },
  scrollTo: function (value) {
    var self = this;
    value = value || 0;

    self.sendMessage(window.parent, {
      scroll: value
    });
  },
  sendMessage: function (target, data) {
    try {
      target.postMessage( JSON.stringify({
        mbData: data
      }), '*');
    } catch (e) {
      console.warn('Unable to send postMessage');
    }
  },
  watchHeight: function () {
    var self = this;

    clearInterval(self.timer);
    self.timer = setInterval(function () {
      self.setSize();
    }, 100);
  },
  pushState: function (url) {
    var self = this;
    self.sendMessage(window.parent, {
      pushState: url
    });
  },
  receiveMessage: function (cb) {
    var addListener = require("addEventListener");

    addListener(window, "message", function (message) {
      cb(message);
    });
  }
};
