var gulp   = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('uglify', function () {
  return gulp.src('build/mashbox.js')
    .pipe(uglify())
    .pipe(rename('mashbox.min.js'))
    .pipe(gulp.dest('build'));
});