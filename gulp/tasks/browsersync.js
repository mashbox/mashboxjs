var gulp        = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
    ui: false,
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: ['examples', 'build']
    },
    files: [
      'examples/**',
      // Exclude Map files
      '!examples/**.map'
    ]
  });
});
