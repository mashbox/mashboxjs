var gulp = require('gulp');
var browserify = require('browserify');
var watchify   = require('watchify');
var source     = require('vinyl-source-stream');
var livereload     = require('gulp-livereload');

var bundler = browserify({
      // Required watchify args
      cache: {}, packageCache: {}, fullPaths: true,
      // Specify the entry point of your app
      entries: './src/main.js',
      // Enable source maps!
      debug: true
    });

var rebundle = function () {
  return bundler
    .bundle()
    .pipe(source('mashbox.js'))
    .pipe(gulp.dest('./build'))
    .pipe(livereload());
}
 
// Basic usage 
gulp.task('watchify', function() {
  bundler = watchify(bundler);
  bundler.on('update', rebundle);

  return rebundle();
});