var gulp   = require('gulp');
var gzip = require('gulp-gzip');

gulp.task('gzip', function () {
  return gulp.src('build/**/*.js')
    .pipe(gzip())
    .pipe(gulp.dest('build'));
});