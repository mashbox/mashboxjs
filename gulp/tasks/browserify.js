var gulp = require('gulp');
var browserify = require('browserify');
var source     = require('vinyl-source-stream');
 
// Basic usage 
gulp.task('browserify', function() {
    return browserify({
        // Required watchify args
        cache: {}, packageCache: {}, fullPaths: false,
        // Specify the entry point of your app
        entries: './src/main.js',
        // Enable source maps!
        debug: false
      })
      .bundle()
      .pipe(source('mashbox.js'))
      .pipe(gulp.dest('./build'));
});