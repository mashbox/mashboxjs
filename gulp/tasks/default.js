// automaticall install packages package.json or bower.json
var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function () {
  runSequence('watchify', 'browser-sync');
});

gulp.task('build', function () {
  runSequence('browserify', 'uglify');
});