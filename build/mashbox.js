(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function (obj, evt, fn) {
  if ('addEventListener' in window) {
      obj.addEventListener(evt, fn, false);

    } else if ('attachEvent' in window){
      obj.attachEvent('on'+ evt, fn);
    }
};
},{}],2:[function(require,module,exports){
// This will render an application in a frame

Math.easeInOutQuad = function (t, b, c, d) {
  t /= d/2;
  if (t < 1) { return c/2*t*t + b; }
  t--;
  return -c/2 * (t*(t-2) - 1) + b;
};

function Application($el) {
  var self = this;
  var config= {
    domain: "mashbox.com"
  };

  self.$   = require('bling');
  self.$el = $el;
  self.app = $el.getAttribute("data-app");

  self.params = {};
  self.queryString = [];

  Array.prototype.slice.call($el.attributes).forEach(function(item) {
    self.params[item.name.replace('data-', '')] = item.value;

    if (item.name.indexOf('data-') > -1 && item.name !== 'data-app') {
      self.queryString.push(item.name.replace('data-', '') + "=" + item.value);
    }
  });

  if (window.location.search.substr(1)) {
    self.queryString.unshift(window.location.search.substr(1));
  }

  self.queryString.push("origin=" + encodeURIComponent(window.location.href));

  if (self.app) {
    var split = self.app.split("/");
    if (split.length > 1) {
      self.customer = split[0];
      self.project = split[1];
      self.page = split[2] || "";
      self.path = self.getPath("//" + self.customer + "." + config.domain + "/" + self.project + '/' + self.page, self.queryString);

    } else {
      self.customer = "";
      self.project = split[0];
      self.path = self.getPath("helper/" + self.project, self.queryString);

    }

  } else {
    self.app = $el.getAttribute("data-test");
    self.path = self.getPath(self.app, self.queryString);
  }

  // Render the frame
  self.render();

  // Setup events
  self.eventHandler();

  return self;
}

Application.prototype.getPath = function (domain, queryString) {
  return domain + '?' + queryString.join('&');
};

Application.prototype.render = function () {
  var self = this;
  var tmpl = "<iframe src='" + self.path + "' scrolling='no' frameborder='0' allowfullscreen seamless></iframe>";

  if (!self.$el.find('iframe').length) {
    // Insert the frame
    self.$el.innerHTML = tmpl;
  }

  // Set the initial sizing
  self.frame = self.$el.find('iframe')[0];
  self.frame.style.width = self.params.width || '100%';
  self.frame.style.height = self.params.height || '300px';
};

Application.prototype.eventHandler = function () {
  var self = this;
  var addListener = require("addEventListener");

  addListener(window, "message", function (message) {
    self.receiveMessage(message);
  });
};

Application.prototype.sendMessage = function (message) {
  var self = this;

  window.Mashbox.Messages.sendMessage(self.frame.contentWindow, message);
};

Application.prototype.receiveMessage = function (message) {
  var self = this;

  try {
    var mbData = JSON.parse(message.data).mbData || {};

    // Resize the height
    if (mbData.height && self.frame) {
      self.frame.style.height = String(mbData.height).indexOf("%") > -1 ? mbData.height : parseInt(mbData.height, 10) + 'px';
      return;
    }

    // Scroll
    if (mbData.hasOwnProperty("scroll") && self.frame) {
      self.scrollTo(mbData.scroll, 600);
      return;
    }

    // Push State
    if (mbData.hasOwnProperty("pushState") && self.frame) {
      if (typeof(window.history.pushState) == 'function') {
        history.pushState(null, null, mbData.pushState);
      } else {
        window.location.href = mbData.pushState; 
      }
      return;
    }

  } catch (e) {
    console.warn("There was a problem with the frame message", e);
  }
};

Application.prototype.scrollTo = function (to, duration) {
    var start = document.documentElement.scrollTop || document.body.scrollTop;
    var change = to - start;
    var currentTime = 0;
    var increment = 20;

    var animateScroll = function (){
      currentTime += increment;
      var val = Math.easeInOutQuad(currentTime, start, change, duration);
      if (document.documentElement) {
        document.documentElement.scrollTop = val;
      }

      if (document.body) {
        document.body.scrollTop = val;
      }

      if (currentTime < duration) {
        setTimeout(animateScroll, increment);
      }
    };
    animateScroll();
};

module.exports = Application;

},{"addEventListener":1,"bling":3}],3:[function(require,module,exports){
(function () {
  var $ = document.querySelectorAll.bind(document);

  Node.prototype.find = function(name) {
    return this.querySelectorAll(name);
  };

  Node.prototype.on = window.on = function(name, fn) {
    this.addEventListener(name, fn);
  };

  NodeList.prototype.__proto__ = Array.prototype;

  NodeList.prototype.on = NodeList.prototype.addEventListener = function(name, fn) {
    this.forEach(function(elem, i) {
      elem.on(name, fn);
    });
  };

  NodeList.prototype.find = function(name) {
    var results = [];
    this.forEach(function(elem, i) {
      elem.find(name).forEach(function(child, i) {
        results.push(child);
      });
    });
    return results;
  };

  // Requirify
  if (typeof define === 'function' && define.amd) {
    define([], $);

    // Browserify
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = $;

  }

})();
},{}],4:[function(require,module,exports){
(function() {
  "use strict";
  // The public function name defaults to window.docReady
  // but you can modify the last line of this function to pass in a different object or method name
  // if you want to put them in a different namespace and those will be used instead of 
  // window.docReady(...)
  var readyList = [];
  var readyFired = false;
  var readyEventHandlersInstalled = false;

  // call this when the document is ready
  // this function protects itself against being called more than once
  function ready() {
    if (!readyFired) {
      // this must be set to true before we start calling callbacks
      readyFired = true;
      for (var i = 0; i < readyList.length; i++) {
        // if a callback here happens to add new ready handlers,
        // the docReady() function will see that it already fired
        // and will schedule the callback to run right after
        // this event loop finishes so all handlers will still execute
        // in order and no new ones will be added to the readyList
        // while we are processing the list
        readyList[i].fn.call(window, readyList[i].ctx);
      }
      // allow any closures held by these functions to free
      readyList = [];
    }
  }

  function readyStateChange() {
    if (document.readyState === "complete") {
      ready();
    }
  }

  // This is the one public interface
  // docReady(fn, context);
  // the context argument is optional - if present, it will be passed
  // as an argument to the callback
  window.docReady = function(callback, context) {
    // if ready has already fired, then just schedule the callback
    // to fire asynchronously, but right away
    if (readyFired) {
      setTimeout(function() {
        callback(context);
      }, 1);
      return;
    } else {
      // add the function and context to the list
      readyList.push({
        fn: callback,
        ctx: context
      });
    }
    // if document already ready to go, schedule the ready function to run
    // IE only safe when readyState is "complete", others safe when readyState is "interactive"
    if (document.readyState === "complete" || (!document.attachEvent && document.readyState === "interactive")) {
      setTimeout(ready, 1);
    } else if (!readyEventHandlersInstalled) {
      // otherwise if we don't have event handlers installed, install them
      if (document.addEventListener) {
        // first choice is DOMContentLoaded event
        document.addEventListener("DOMContentLoaded", ready, false);
        // backup is window load event
        window.addEventListener("load", ready, false);
      } else {
        // must be IE
        document.attachEvent("onreadystatechange", readyStateChange);
        window.attachEvent("onload", ready);
      }
      readyEventHandlersInstalled = true;
    }
  };

  // Requirify
  if (typeof define === 'function' && define.amd) {
    define([], window.docReady);

    // Browserify
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = window.docReady;

  }
})();
},{}],5:[function(require,module,exports){
/*! MashboxJS 0.1.8 | (c) 2016 Mashbox LLC */
(function(window, document) {

  docReady = require('docready');

  // Set the global variable
  function Mashbox() {
    var self = this;

    self.$ = require('bling');

    // Add the App renderer
    self.App = require('app');

    // Add the Poller
    self.Poller = require('poller');

    // Add the Messages
    self.Messages = require('messages');

    // Set the Active Applications for reference later
    self.activeApplications = [];

    // Render the Apps
    window.docReady(function() {
      var $apps = self.$('.mashbox-app');
      if ($apps.length) {
        for (var x = 0; x < $apps.length; x = x + 1) {
          self.activeApplications.push(new self.App($apps[x]));
        }
      }

      // Trigger a loaded event
      if (typeof (window.MashboxLoaded) === 'function') {
        window.MashboxLoaded.call(self);
      }
    });

    return self;
  }

  window.Mashbox = new Mashbox();

  // Requirify
  if (typeof define === 'function' && define.amd) {
    define([], window.Mashbox);

    // Browserify
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = window.Mashbox;

  }

})(window, document);

},{"app":2,"bling":3,"docready":4,"messages":6,"poller":7}],6:[function(require,module,exports){
module.exports = {
  setSize: function (height) {
    var self = this;
    self.frameHeight = 0;

    var getComputedStyle = function (prop,el) {
  		function convertUnitsToPxForIE8(value) {
  			var PIXEL = /^\d+(px)?$/i;

  			if (PIXEL.test(value)) {
  				return parseInt(value, 10);
  			}

  			var style = el.style.left;
  			var runtimeStyle = el.runtimeStyle.left;

  			el.runtimeStyle.left = el.currentStyle.left;
  			el.style.left = value || 0;
  			value = el.style.pixelLeft;
  			el.style.left = style;
  			el.runtimeStyle.left = runtimeStyle;

  			return value;
  		}

  		var retVal = 0;
  		el =  el || document.body;

  		/* istanbul ignore else */ // Not testable in phantonJS
  		if (('defaultView' in document) && ('getComputedStyle' in document.defaultView)) {
  			retVal = document.defaultView.getComputedStyle(el, null);
  			retVal = (null !== retVal) ? retVal[prop] : 0;
  		} else {//IE8
  			retVal =  convertUnitsToPxForIE8(el.currentStyle[prop]);
  		}

  		return parseInt(retVal, 10);
  	};

    height = height || Math.max(document.body.offsetHeight + getComputedStyle('marginTop', document.body) + getComputedStyle('marginBottom', document.body), (document.documentElement.scrollHeight || document.body.scrollHeight));

    if (self.frameHeight !== height) {
      self.sendMessage(window.parent, {
        height: height
      });
    }
  },
  scrollTo: function (value) {
    var self = this;
    value = value || 0;

    self.sendMessage(window.parent, {
      scroll: value
    });
  },
  sendMessage: function (target, data) {
    try {
      target.postMessage( JSON.stringify({
        mbData: data
      }), '*');
    } catch (e) {
      console.warn('Unable to send postMessage');
    }
  },
  watchHeight: function () {
    var self = this;

    clearInterval(self.timer);
    self.timer = setInterval(function () {
      self.setSize();
    }, 100);
  },
  pushState: function (url) {
    var self = this;
    self.sendMessage(window.parent, {
      pushState: url
    });
  },
  receiveMessage: function (cb) {
    var addListener = require("addEventListener");

    addListener(window, "message", function (message) {
      cb(message);
    });
  }
};

},{"addEventListener":1}],7:[function(require,module,exports){
// This is for getting data from api.mashbox.com

module.exports = {};
},{}]},{},[5]);
